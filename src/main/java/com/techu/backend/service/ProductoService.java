package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.UserModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoService {

    private List<ProductoModel> productoList = new ArrayList<>();

    public ProductoService(){
        productoList.add(new ProductoModel("1","producto 1", 100.50));
        productoList.add(new ProductoModel("2","producto 2", 90.50));
        productoList.add(new ProductoModel("3","producto 3", 88.40));
        productoList.add(new ProductoModel("4","producto 4", 110.20));
        productoList.add(new ProductoModel("5","producto 5", 130.00));
        List<UserModel> users = new ArrayList<>();
        users.add(new UserModel("1"));
        users.add(new UserModel("3"));
        users.add(new UserModel("5"));
        // le metemos al id = 2 los 3 usuarios.
        productoList.get(1).setUsers(users);
    }

    //READ productos
    public List<ProductoModel> getProductos(){
        return productoList;
    }

    //READ instance (por id)
    public ProductoModel getProductoById(String id){
        int idx = Integer.parseInt(id);
        if(getIndex(idx)>=0){
            return productoList.get(getIndex(idx));
        }
        return null;
    }

    //CREATE productos
    public ProductoModel addProducto(ProductoModel nuevoProducto){
        productoList.add(nuevoProducto);
        return nuevoProducto;
    }

    //Update
    public ProductoModel updateProductoById(String id, ProductoModel newPro){
        int idx = Integer.parseInt(id);
        int index = getIndex(idx);
        if(index>=0) {
            productoList.set(index, newPro);
            return productoList.get(index);
        }
        return null;
    }

    //Update patch precio
    public ProductoModel updateProductoPrecioById(String id, double newPrecio){
        int idx = Integer.parseInt(id);
        int pos = getIndex(idx);

        // otra forma de hacerlo
        // productoList.get(pos).setPrecio(newPrecio);

        if(pos>=0) {
            ProductoModel pr = productoList.get(pos);
            pr.setPrecio(newPrecio);
            productoList.set(pos, pr);
            return productoList.get(pos);
        }
        return null;
    }

    //Delete
    public void removeProductoById(String id){
        //int pos = productoList.indexOf(productoList.get(index));
        int idx = Integer.parseInt(id);
        int index = getIndex(idx);
        if(index>=0) {
            productoList.remove(index);
        }
    }

    // Devuelve la posición de un producto en la coleccion
    public int getIndex(int index){
        //dado un id de producto devuelve la posicion donde se encuentra
        int i=0;
        while(i<productoList.size()){
            if(Integer.parseInt(productoList.get(i).getId())==index){
                // obtenemos el producto que está en la posición i, y recuperamos el atributo con getId
                return (i);
            }
            i++;
        }
        return -1;
    }
}
