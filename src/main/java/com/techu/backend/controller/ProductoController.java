package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.ProductoPrecioModel;
import com.techu.backend.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping("")
    public String index(){
        return "API REST Tech U! v1.0.0";
    }

    //GET a todos los productos (Coleccion)
    @GetMapping("/products")
    public List<ProductoModel> getProductos(){
        return productoService.getProductos();
    }

    //GET a un producto concreto por id (Instancia)
    @GetMapping("/products/{id}")
    public ResponseEntity getProductoById(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if (pr==null){
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        //return ResponseEntity.ok(pr); este metodo, a traves de una clase builder, crea un objeto y envia la respuesta
        return new ResponseEntity(pr, HttpStatus.OK);
    }

    //POST para crear un producto
    @PostMapping("/products")
    public ResponseEntity<String> postProducto(@RequestBody ProductoModel newProduct){
        productoService.addProducto(newProduct);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    //PUT para actualizar un producto Total
    @PutMapping("/products/{id}")
    public ResponseEntity putProductoById(@PathVariable String id,
                                  @RequestBody ProductoModel productoModel){
        // Cambiar para no tener que hacer el parseo, que el metodo reciba dirctamente un string
        ProductoModel pr = productoService.getProductoById(id);
        if (pr==null){
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        productoService.updateProductoById(id,productoModel);
        return new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK);
    }

    //DELETE para borrar un producto aunque no está definido en el Codelab
    @DeleteMapping("/products/{id}")
    public ResponseEntity deleteProductoById(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if(pr==null){
            return new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        productoService.removeProductoById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT); //204, pero no saldra mensaje
    }

    //PATCH para actualizar un producto Parcial
    @PatchMapping("/products/{id}")
    public ResponseEntity patchProductoById(@PathVariable String id,
                                    @RequestBody ProductoPrecioModel productoPrecioModel){
        ProductoModel pr = productoService.getProductoById(id);
        if (pr==null){
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        //puede ser una solucion
        // pr.setPrecio(productoPrecioModel.getPrecio());
        //productoService.updateProductoById(id,pr);
        // o bien crear un metodo updateProductoPrevioById
        productoService.updateProductoPrecioById(id,productoPrecioModel.getPrecio());
        return new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK);
    }

    //GET a un subrecurso users
    @GetMapping("/products/{id}/users")
    public ResponseEntity getUsers(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if (pr==null){
            //no existe el producto
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null){
            // tiene usuarios
            return ResponseEntity.ok(pr.getUsers());
        }
        else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
